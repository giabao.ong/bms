package core

import (
	"github.com/globalsign/mgo"
)

type ConfigDB struct {
	User     string
	Password string
	Addr     string
	Port     int
	DBName   string
}

type DBModel struct {
	DBName         string
	url            string
	session        *mgo.Session
	TemplateObject interface{}
	ColName        string
	collection     *mgo.Collection
	db             *mgo.Database
	callback       CallBack
}

type DBSession struct {
	Session *mgo.Session
	DBName	string
}

type CallBack = func(session *DBSession)