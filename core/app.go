package core

import (
	"errors"
	"fmt"
)

type App struct {
	server  APIServer
	listDB  []*DBModel
	AppName string
	Addr    string
}

func (s *App) SetupProtocolServer(protocol string) APIServer {
	var server APIServer
	switch protocol {
	case "HTTP":
		server = newHTTPAPIServer(s.AppName + " HTTP")
	case "gRPC":
		server = newGRPCServer(s.AppName + " gRPC")
	}

	s.server = server

	return server
}


func (s *App) DBInit(conf *ConfigDB, cb CallBack) error {
	if conf == nil {
		panic("Config DB nill")
	}
	if conf.DBName == "" {
		panic(errors.New("Invalid config DB"))
	}
	if conf.Port <= 0 {
		conf.Port = 27017
	}

	a := ""
	if conf.Password != "" || conf.User != "" {
		a = ":@"
	}

	url := fmt.Sprintf("mongodb://%v%v%v%v:%v/%v?readPreference=primary&ssl=false",
		conf.User, conf.Password, a, conf.Addr, conf.Port, conf.DBName)

	db := &DBModel{
		DBName:   conf.DBName,
		url:      url,
		callback: cb,
	}
	s.listDB = append(s.listDB, db)
	return nil
}

func (s *App) Launch() error {
	println("+ Launching...")

	for i, _ :=range s.listDB {
		println(fmt.Sprintf("+ Connecting DB: %v", s.listDB[i].DBName))
		err := s.listDB[i].connect()
		if err != nil {
			println(err.Error())
			return err
		}
	}

	if s.server != nil {
		err := s.server.start()
		return err
	}

	return nil
}