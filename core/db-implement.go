package core

import (
	"fmt"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"reflect"
	"strconv"
	"time"
)

func (db *DBModel) connect() error {
	session, err := mgo.Dial(db.url)
	if err != nil {
		return err
	}

	println(fmt.Sprintf("+ Connected DB: %v", db.DBName))

	db.callback(&DBSession{Session: session, DBName: db.DBName})
	return err
}

func (db *DBModel) Init(session *DBSession) {
	if session == nil {
		panic("Session is nil")
	}

	db.session = session.Session
	if db.DBName != session.DBName {
		db.DBName = session.DBName
	}
	db.db = session.Session.DB(db.DBName)
	db.collection = db.db.C(db.ColName)
}

func (db *DBModel) Query(query interface{}, limit, offset int, reverse bool) *Response {
	session := db.session.Copy()
	defer session.Close()
	cols := db.collection

	if limit < 0 {
		limit = 1
	} else if limit == 0 {
		limit = 1500
	}
	if offset < 0 {
		offset = 0
	}

	q := cols.Find(query)
	q.Skip(offset)

	if limit > 0 {
		q.Limit(limit)
	}
	if reverse {
		q.Sort("-_id")
	}

	input := db.initListObj(limit)
	err := q.All(&input)
	if err != nil {
		return &Response{
			Status:    APIStatus.Error,
			Data:      nil,
			Message:   err.Error(),
			ErrorCode: "DB_ERROR",
		}
	}

	if reflect.ValueOf(input).Len() == 0 {
		return &Response{
			Status:    APIStatus.NotFound,
			Data:      input,
			Message:   "Query " + db.ColName + " not found",
			ErrorCode: "NOT_FOUND",
		}
	}

	return &Response{
		Status:    APIStatus.Ok,
		Data:      input,
		Message:   "Query " + db.ColName + " successfully",
		ErrorCode: "",
		Total:     reflect.ValueOf(input).Len(),
	}
}

func (db *DBModel) QuerySort(query interface{}, limit, offset int, fields ...string) *Response {
	session := db.session.Copy()
	defer session.Close()
	cols := db.collection

	if limit < 0 {
		limit = 1
	} else if limit == 0 {
		limit = 1500
	}
	if offset < 0 {
		offset = 0
	}

	q := cols.Find(query)
	q.Skip(offset)

	if limit > 0 {
		q.Limit(limit)
	}
	q.Sort(fields...)

	input := db.initListObj(limit)
	err := q.All(&input)
	if err != nil {
		return &Response{
			Status:    APIStatus.Error,
			Data:      nil,
			Message:   err.Error(),
			ErrorCode: "DB_ERROR",
		}
	}

	if reflect.ValueOf(input).Len() == 0 {
		return &Response{
			Status:    APIStatus.NotFound,
			Data:      input,
			Message:   "Query " + db.ColName + " not found",
			ErrorCode: "NOT_FOUND",
		}
	}

	return &Response{
		Status:    APIStatus.Ok,
		Data:      input,
		Message:   "Query " + db.ColName + " successfully",
		ErrorCode: "",
		Total:     reflect.ValueOf(input).Len(),
	}
}

func (db *DBModel) Create(model interface{}) *Response {
	session := db.session.Copy()
	defer session.Close()

	if model == nil {
		return &Response{
			Status:    APIStatus.Error,
			Message:   "Your model is nil",
			ErrorCode: "DB_ERROR",
		}
	}

	bytes, err := bson.Marshal(model)
	if err != nil {
		return &Response{
			Status:    APIStatus.Error,
			Message:   err.Error(),
			ErrorCode: "MAP_DATA",
		}
	}

	obj := bson.M{}
	err = bson.Unmarshal(bytes, &obj)
	if err != nil {
		return &Response{
			Status:    APIStatus.Error,
			Message:   err.Error(),
			ErrorCode: "MAP_DATA",
		}
	}

	obj["created_time"] = time.Now()
	obj["last_updated_time"] = time.Now()

	err = db.collection.Insert(obj)
	if err != nil {
		return &Response{
			Status:    APIStatus.Error,
			Message:   err.Error(),
			ErrorCode: "DB_ERROR",
		}
	}

	entity := db.initObject()
	bytes, err = bson.Marshal(obj)
	if err != nil {
		return &Response{
			Status:    APIStatus.Error,
			Message:   err.Error(),
			ErrorCode: "MAP_DATA",
		}
	}

	err = bson.Unmarshal(bytes, entity)
	if err != nil {
		return &Response{
			Status:    APIStatus.Error,
			Message:   err.Error(),
			ErrorCode: "MAP_DATA",
		}
	}
	list := db.initListObj(1)
	listV := reflect.Append(reflect.ValueOf(list), reflect.Indirect(reflect.ValueOf(entity)))
	return &Response{
		Status:    APIStatus.Ok,
		Data:      listV.Interface(),
		Message:   "Created " + db.ColName + " successfully",
		ErrorCode: "",
		Header:    nil,
		Total:     1,
	}
}

func (db *DBModel) CreateMany(model ...interface{}) *Response {
	session := db.session.Copy()
	defer session.Close()

	if model == nil || len(model) == 0 {
		return &Response{
			Status:    APIStatus.Error,
			Message:   "Your model is nil",
			ErrorCode: "DB_ERROR",
		}
	}

	list := db.initListObj(len(model))
	listV := reflect.ValueOf(list)

	objs := []bson.M{}
	inters := []interface{}{}

	for i, _ := range model {
		bytes, err := bson.Marshal(model[i])
		if err != nil {
			return &Response{
				Status:    APIStatus.Error,
				Message:   err.Error(),
				ErrorCode: "MAP_DATA",
			}
		}
		obj := bson.M{}
		err = bson.Unmarshal(bytes, &obj)
		if err != nil {
			return &Response{
				Status:    APIStatus.Error,
				Message:   err.Error(),
				ErrorCode: "MAP_DATA",
			}
		}

		obj["created_time"] = time.Now()
		obj["last_updated_time"] = time.Now()

		objs = append(objs, obj)
		inters = append(inters, obj)
	}

	err := db.collection.Insert(inters...)
	if err != nil {
		return &Response{
			Status:    APIStatus.Error,
			Message:   err.Error(),
			ErrorCode: "DB_ERROR",
		}
	}

	for _, obj := range objs {
		entity := db.initObject()
		bytes, err := bson.Marshal(obj)
		if err != nil {
			return &Response{
				Status:    APIStatus.Error,
				Message:   err.Error(),
				ErrorCode: "MAP_DATA",
			}
		}

		err = bson.Unmarshal(bytes, entity)
		if err != nil {
			return &Response{
				Status:    APIStatus.Error,
				Message:   err.Error(),
				ErrorCode: "MAP_DATA",
			}
		}

		listV = reflect.Append(listV, reflect.Indirect(reflect.ValueOf(entity)))
	}

	return &Response{
		Status:  APIStatus.Ok,
		Data:    listV.Interface(),
		Message: "Created " + db.ColName + " successfully",
		Header:  nil,
	}
}

func (db *DBModel) Update(filter interface{}, updater interface{}) *Response {
	session := db.session.Copy()
	defer session.Close()

	bytes, err := bson.Marshal(updater)
	if err != nil {
		return &Response{
			Status:  APIStatus.Error,
			Message: "Failed: " + err.Error(),
		}
	}

	obj := bson.M{}
	err = bson.Unmarshal(bytes, &obj)

	obj["last_updated_time"] = time.Now()

	info, err := db.collection.UpdateAll(filter, bson.M{"$set": obj})
	if err != nil {
		return &Response{
			Status:  APIStatus.Error,
			Message: "Failed: " + err.Error(),
		}
	}

	if info.Updated == 0 {
		return &Response{
			Status:  APIStatus.NotFound,
			Message: "Not match " + db.ColName,
		}
	}

	return &Response{
		Status:  APIStatus.Ok,
		Message: "Updated " + db.ColName + "successfully with " + strconv.Itoa(info.Updated) + " entities",
		Total:   info.Updated,
	}
}

func (db *DBModel) UpdateOne(filter interface{}, updater interface{}) *Response {
	session := db.session.Copy()
	defer session.Close()

	bytes, err := bson.Marshal(updater)
	if err != nil {
		return &Response{
			Status:    APIStatus.Error,
			Message:   err.Error(),
			ErrorCode: "MAP_DATA",
		}
	}

	update := bson.M{}
	result := bson.M{}
	err = bson.Unmarshal(bytes, &update)
	if err != nil {
		return &Response{
			Status:    APIStatus.Error,
			Message:   err.Error(),
			ErrorCode: "MAP_DATA",
		}
	}

	update["last_updated_time"] = time.Now()

	change := mgo.Change{
		Update:    bson.M{"$set": update},
		ReturnNew: true,
	}

	q := db.collection.Find(filter)
	q.Limit(1)
	info, err := q.Apply(change, &result)
	if err != nil {
		if err.Error() == "not found" {
			return &Response{
				Status:    APIStatus.NotFound,
				Message:   "Not match any " + db.ColName,
				ErrorCode: "NOT_FOUND",
			}
		}

		return &Response{
			Status:    APIStatus.Error,
			Message:   err.Error(),
			ErrorCode: "DB_ERROR",
		}
	}
	entity := db.initObject()

	if info.Updated > 0 {
		bytes, err := bson.Marshal(result)
		if err != nil {
			return &Response{
				Status:    APIStatus.Error,
				Message:   err.Error(),
				ErrorCode: "MAP_DATA",
			}
		}

		err = bson.Unmarshal(bytes, entity)
		if err != nil {
			return &Response{
				Status:    APIStatus.Error,
				Message:   err.Error(),
				ErrorCode: "MAP_DATA",
			}
		}
		list := db.initListObj(1)
		listV := reflect.Append(reflect.ValueOf(list), reflect.Indirect(reflect.ValueOf(entity)))

		return &Response{
			Status:  APIStatus.Ok,
			Data:    listV.Interface(),
			Message: "Updated " + db.ColName + "successfully with " + strconv.Itoa(info.Updated) + " entities",
		}
	}

	list := db.initListObj(1)
	listV := reflect.Append(reflect.ValueOf(list), reflect.Indirect(reflect.ValueOf(entity)))
	return &Response{
		Status:    APIStatus.NotFound,
		Data:      listV.Interface(),
		Message:   "Not match " + db.ColName,
		ErrorCode: "NOT_FOUND",
		Header:    nil,
		Total:     0,
	}
}

func (db *DBModel) Delete(filter interface{}) *Response {
	session := db.session.Copy()
	defer session.Close()

	info, err := db.collection.RemoveAll(filter)
	if err != nil {
		return &Response{
			Status:    APIStatus.Error,
			Message:   err.Error(),
			ErrorCode: "DB_ERROR",
		}
	}

	if info.Removed > 0 {
		return &Response{
			Status:  APIStatus.Ok,
			Message: "Deleted " + strconv.Itoa(info.Removed) + " " + db.ColName,
		}
	} else if info.Removed == 0 {
		return &Response{
			Status:    APIStatus.NotFound,
			Message:   "Not found " + db.ColName,
			ErrorCode: "NOT_FOUND",
		}
	}

	return &Response{
		Status:    APIStatus.Error,
		Message:   "Delete " + db.ColName + " failed",
		ErrorCode: "DB_ERROR",
	}
}

func (db *DBModel) Count(filter interface{}) *Response {
	session := db.session.Copy()
	defer session.Close()

	q := db.collection.Find(filter)
	total, err := q.Count()
	if err != nil {
		if err.Error() == "not found" {
			return &Response{
				Status:    APIStatus.NotFound,
				Message:   "Not match any " + db.ColName,
				ErrorCode: "NOT_FOUND",
				Total:     0,
			}
		}
		return &Response{
			Status:    APIStatus.Error,
			Data:      nil,
			Message:   "Counted failed",
			ErrorCode: "DB_ERROR",
		}
	}

	return &Response{
		Status:  APIStatus.Ok,
		Data:    nil,
		Message: "Counted " + db.ColName + " successfully",
		Total:   total,
	}
}

func (db *DBModel) Distinct(field string, filter interface{}, result interface{}) error {
	session := db.session.Copy()
	defer session.Close()

	return db.collection.Find(filter).Distinct(field, result)
}

func (db *DBModel) GetCol() *mgo.Collection {
	return db.collection
}

func (db *DBModel) initListObj(limit int) interface{} {
	t := reflect.TypeOf(db.TemplateObject)
	return reflect.MakeSlice(reflect.SliceOf(t), 0, limit).Interface()
}

func (db *DBModel) initObject() interface{} {
	t := reflect.TypeOf(db.TemplateObject)
	v := reflect.New(t)
	return v.Interface()
}

func (db *DBModel) CreateIndex(index mgo.Index) error {
	s := db.session.Copy()
	defer s.Close()
	col := db.collection
	err := col.EnsureIndex(index)
	return err
}
