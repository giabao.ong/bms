package core

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"reflect"
)

func (s *HTTPServer) SetHandler(method MethodValue,url string, handler Handler) {
	http.HandleFunc(url, func(writer http.ResponseWriter, request *http.Request) {
		var req APIRequest
		api := &apiModel{
			w:   writer,
			req: request,
		}
		req = api
		var res APIResponse
		res = api

		if s.preRequestHandle != nil {
			err := s.preRequestHandle(req,res)
			if err != nil {
				r := Response{
					Status: "Error",
					Data:   nil,
					Message: err.Error(),
				}
				writer.WriteHeader(500)
				json.NewEncoder(writer).Encode(r)
				return
			}
		}


		if request.Method != method.Value {
			r := Response{
				Status: "NOT_ALLOWED",
				Data:   nil,
			}
			writer.WriteHeader(405)
			json.NewEncoder(writer).Encode(r)
			return
		}
		handler(req, res)
	})
}

func (a *apiModel) GetBody() string {
	bytes, err := ioutil.ReadAll(a.req.Body)
	if err != nil {
		return ""
	}
	defer a.req.Body.Close()
	return string(bytes)
}

func (a *apiModel) ParseBodyTo(i interface{}) error {
	if a.req.Body == nil || reflect.TypeOf(a.req.Body) == nil{
		return errors.New("Body nil")
	}

	bytes, err := ioutil.ReadAll(a.req.Body)
	if err != nil {
		return err
	}

	return json.Unmarshal(bytes, i)
}

func (a *apiModel) GetParam(q string) string {
	query := a.req.URL.Query().Get(q)
	return query
}

func (a *apiModel) GetUserAgent() string {
	return a.req.UserAgent()
}

func (a *apiModel) GetHeader(key string) string {
	return a.req.Header.Get(key)
}

func (a *apiModel) GetHeaders() map[string]string {
	h := map[string]string{}
	rHeaders := a.req.Header
	for k := range rHeaders {
		h[k] = rHeaders.Get(k)
	}
	return h
}

func (a *apiModel) Resp(response *Response) error {
	if response.Header != nil {
		for i, v := range response.Header {
			a.w.Header().Add(i, v)
		}
	}

	a.w.Header().Set("Content-Type", "application/json")
	a.w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	a.w.Header().Set("Access-Control-Allow-Headers",
		"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	a.w.Header().Set("Access-Control-Allow-Origin", "*")

	switch response.Status {
	case APIStatus.Ok:
		a.httpCode = 200
		a.w.WriteHeader(200)
	case APIStatus.Error:
		a.httpCode = 500
		a.w.WriteHeader(500)
	case APIStatus.Unauthorized:
		a.httpCode = 401
		a.w.WriteHeader(401)
	case APIStatus.Invalid:
		a.httpCode = 400
		a.w.WriteHeader(400)
	case APIStatus.NotFound:
		a.httpCode = 404
		a.w.WriteHeader(404)
	default:
		a.httpCode = 200
		a.w.WriteHeader(200)
	}

	return json.NewEncoder(a.w).Encode(response)
}

func (api *apiModel) GetHTTPCode() int {
	return api.httpCode
}

func (api *apiModel) GetResponse() *Response {
	return nil
}