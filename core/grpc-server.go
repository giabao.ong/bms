package core

import (
	"encoding/json"
	"errors"
	"fmt"
	core_grpc "gitlab.com/giabao.ong/bms/core/grpc"
	"google.golang.org/grpc"
	"io"
	"log"
	"net"
)

type GRPCServer struct {
	Port             int
	Protocol         string
	preRequestHandle Handler
	options          []interface{}
	ServerName       string
	router           map[string]Handler
}

type APIgRPCRequest struct {
	response core_grpc.Response
	content *core_grpc.APIRequest
}

type APIgRPCResponse struct {
	content core_grpc.GRPCServer_CallApiServer
	responser *Response
	httpCode int
}

func newGRPCServer(serveName string) APIServer {
	newGRPCServe := &GRPCServer{
		Protocol:         "gRPC",
		ServerName:       serveName,
	}

	newGRPCServe.router = make(map[string]Handler)

	return newGRPCServe
}

func newAPIgRPCRequest(request *core_grpc.APIRequest) APIRequest {
	return &APIgRPCRequest{content: request}
}

func newAPIgRPCResponse(input core_grpc.GRPCServer_CallApiServer) APIResponse {
	return &APIgRPCResponse{
		content: input,
	}
}

func (rpc *GRPCServer) PreRequest(handler Handler) {
	rpc.preRequestHandle = handler
}

func (rpc *GRPCServer) SetHandler(method MethodValue, url string, function Handler){
	key := fmt.Sprintf("%s:%s",method, url)
	rpc.router[key] = function
}

func (rpc *GRPCServer) CallApi(input core_grpc.GRPCServer_CallApiServer) error {
	for {
		r, err := input.Recv()
		if err != nil {

			if err == io.EOF {
				return nil
			}
			return err
		}

		key := fmt.Sprintf("%s:%s", r.GetMethod(), r.GetPath())
		if handler, ok := rpc.router[key]; ok {
			var (
				req  APIRequest = newAPIgRPCRequest(r)
				resp APIResponse = newAPIgRPCResponse(input)
			)
			rpc.preRequestHandle(req,resp)
			handler(req, resp)
		}
	}
}

func (rpc *GRPCServer) SetPort(port int) {
	rpc.Port = port
}

func (rpc *GRPCServer) start() error{
	serve := grpc.NewServer()
	core_grpc.RegisterGRPCServerServer(serve, new(GRPCServer))

	addr := fmt.Sprintf(":%d", rpc.Port)

	listen, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatal(err)
	}
	println(fmt.Sprintf("[  %s  ] is running on %s" ,rpc.ServerName ,addr))
	return serve.Serve(listen)
}

func (req *APIgRPCRequest) GetBody() string {
	return req.content.GetContent()
}

func (req *APIgRPCRequest) GetParam(k string) string {
	if v, ok := req.content.Params[k]; ok {
		return v
	}

	return ""
}

func (req *APIgRPCRequest) GetParams() map[string]string {
	return req.content.Headers
}

func (req *APIgRPCRequest) GetUserAgent() string {
	if v, ok := req.content.Headers["User-Agent"]; ok {
		return v
	}

	return ""
}

func (req *APIgRPCRequest) GetHeader(k string) string {
	if v, ok := req.content.Headers[k]; ok {
		return v
	}

	return ""
}

func (req *APIgRPCRequest) GetHeaders() map[string]string {
	return req.content.Headers
}

func (req *APIgRPCRequest) ParseBodyTo(i interface{}) error {
	err := json.Unmarshal([]byte(req.content.Content), i)
	return err
}

func (resp *APIgRPCResponse) Resp(response *Response) error {
	if response == nil {
		return errors.New("Response == nil")
	}

	grpcResponse := core_grpc.Response{}

	switch response.Status {
	case APIStatus.Ok:
		grpcResponse.HTTPCode = 200
		resp.httpCode = 200
	case APIStatus.Error:
		resp.httpCode = 500
		grpcResponse.HTTPCode = 500
	case APIStatus.Unauthorized:
		resp.httpCode = 401
		grpcResponse.HTTPCode = 401
	case APIStatus.Invalid:
		resp.httpCode = 400
		grpcResponse.HTTPCode = 400
	case APIStatus.NotFound:
		resp.httpCode = 404
		grpcResponse.HTTPCode = 404
	default:
		resp.httpCode = 200
		grpcResponse.HTTPCode = 200
	}


	bytes, err := json.Marshal(response)
	if err != nil {
		return err
	}
	grpcResponse.Content = string(bytes)
	grpcResponse.ErrorCode = response.ErrorCode
	grpcResponse.Message = response.Message
	grpcResponse.Total = int32(response.Total)
	grpcResponse.Headers = response.Header
	resp.responser = response

	resp.content.Send(&grpcResponse)
	return nil
}

func (resp *APIgRPCResponse) GetHTTPCode() int {
	return resp.httpCode
}

func (resp *APIgRPCResponse) GetResponse() *Response  {
	return resp.responser
}