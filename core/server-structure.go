package core

type HTTPServer struct {
	Port             int
	Protocol         string
	preRequestHandle Handler
	options          []interface{}
	ServerName       string
}

type APIServer interface {
	PreRequest(Handler)
	SetHandler(method MethodValue, url string, function Handler)
	start() error
	SetPort(int)
}
