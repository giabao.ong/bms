package core

import (
	"net/http"
	"net/http/httptest"
)

type apiModel struct {
	w           http.ResponseWriter
	req         *http.Request
	beforeResponse   error
	httpCode	int
}

type apiModelTest struct {
	w           *httptest.ResponseRecorder
	req         *http.Request
	reponse     *Response
	httpCode	int
}

type apiStatus struct {
	Ok           string
	NotFound     string
	Unauthorized string
	Invalid      string
	Error        string
}

var APIStatus = &apiStatus{
	Ok:           "OK",
	NotFound:     "NOT_FOUND",
	Unauthorized: "UNAUTHORIZED",
	Invalid:      "INVALID",
	Error:        "ERROR",
}

type Response struct {
	Status    string            `json:"status"`
	Data      interface{}       `json:"data"`
	Message   string            `json:"message"`
	ErrorCode string            `json:"errorCode"`
	Header    map[string]string `json:"-"`
	Total	int					`json:"total"`
}

type APIResponse interface {
	Resp(*Response) error
	GetHTTPCode() int
	GetResponse() *Response
}

type APIRequest interface {
	GetBody() string
	ParseBodyTo(interface{}) error
	GetParam(string) string
	GetUserAgent() string
	GetHeader(string) string
	GetHeaders() map[string]string
}

type Handler = func(req APIRequest, res APIResponse) error
type ProcessFunc = func(options...interface{})

type MethodValue struct {
	Value string
}

type MethodEnum struct {
	GET     MethodValue
	POST    MethodValue
	PUT     MethodValue
	DELETE  MethodValue
	OPTIONS MethodValue
}

// APIMethod Published enum
var APIMethod = MethodEnum{
	GET:     MethodValue{Value: "GET"},
	POST:    MethodValue{Value: "POST"},
	PUT:     MethodValue{Value: "PUT"},
	DELETE:  MethodValue{Value: "DELETE"},
	OPTIONS: MethodValue{Value: "OPTIONS"},
}