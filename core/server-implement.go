package core

import (
	"fmt"
	"net/http"
	"strconv"
	"time"
)

func newHTTPAPIServer(serveName string) APIServer {
	server := HTTPServer{
		Protocol:         "HTTP",
		ServerName:       serveName,
	}

	return &server
}

func (serve *HTTPServer) SetPort(p int) {
	serve.Port = p
}

func (ser *HTTPServer) start() error {
	if ser.Port < 0 {
		panic("Port < 0")
	}

	addr := ":" + strconv.Itoa(ser.Port)
	s := &http.Server{
		Addr:         addr,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	println(fmt.Sprintf("[  %s  ] is running on %s" ,ser.ServerName ,addr))
	err := s.ListenAndServe()
	return err
}

func (s *HTTPServer) PreRequest(handler Handler) {
	if handler == nil {
		return
	}

	s.preRequestHandle =handler
	return
}