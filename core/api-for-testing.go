package core

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
)

func (a *apiModelTest) GetBody() string {
	bytes, err := ioutil.ReadAll(a.req.Body)
	if err != nil {
		return ""
	}
	defer a.req.Body.Close()
	return string(bytes)
}

func (a *apiModelTest) ParseBodyTo(i interface{}) error {
	if a.req.Body == nil || reflect.TypeOf(a.req.Body) == nil {
		return errors.New("Body nil")
	}

	bytes, err := ioutil.ReadAll(a.req.Body)
	if err != nil {
		return err
	}

	return json.Unmarshal(bytes, i)
}

func (a *apiModelTest) GetParam(q string) string {
	query := a.req.URL.Query()[q]
	return query[0]
}

func (a *apiModelTest) GetUserAgent() string {
	return a.req.UserAgent()
}

func (a *apiModelTest) GetHeader(key string) string {
	return a.req.Header.Get(key)
}

func (a *apiModelTest) GetHeaders() map[string]string {
	h := map[string]string{}
	rHeaders := a.req.Header
	for k := range rHeaders {
		h[k] = rHeaders.Get(k)
	}
	return h
}

func (a *apiModelTest) Resp(response *Response) error {
	if response.Header != nil {
		for i, v := range response.Header {
			a.w.Header().Add(i, v)
		}
	}

	a.w.Header().Set("Content-Type", "application/json")
	a.w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	a.w.Header().Set("Access-Control-Allow-Headers",
		"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	a.w.Header().Set("Access-Control-Allow-Origin", "*")

	switch response.Status {
	case APIStatus.Ok:
		a.httpCode = 200
		a.w.WriteHeader(200)
	case APIStatus.Error:
		a.httpCode = 500
		a.w.WriteHeader(500)
	case APIStatus.Unauthorized:
		a.httpCode = 401
		a.w.WriteHeader(401)
	case APIStatus.Invalid:
		a.httpCode = 400
		a.w.WriteHeader(400)
	case APIStatus.NotFound:
		a.httpCode = 404
		a.w.WriteHeader(404)
	default:
		a.httpCode = 200
		a.w.WriteHeader(200)
	}

	a.reponse = response
	return json.NewEncoder(a.w).Encode(response)
}

func (api *apiModelTest) GetHTTPCode() int {
	return api.httpCode
}

func (api *apiModelTest) GetResponse() *Response {
	return api.reponse
}


func NewMethodAPIForTest(r *http.Request, w *httptest.ResponseRecorder) (APIRequest, APIResponse) {
	var req APIRequest
	api := &apiModelTest{
		w:   w,
		req: r,
	}
	req = api
	var res APIResponse
	res = api

	return req, res
}