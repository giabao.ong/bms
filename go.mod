module gitlab.com/giabao.ong/bms

go 1.12

require (
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	google.golang.org/grpc v1.36.1
	google.golang.org/protobuf v1.26.0
)
