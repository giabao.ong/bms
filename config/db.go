package config

import (
	"gitlab.com/giabao.ong/bms/core"
	"gitlab.com/giabao.ong/bms/model"
)

func OnDBConnected(s *core.DBSession) {
	model.InitDBSeat(s)
	model.DBTicket.Init(s)
	model.DBCinema.Init(s)
	model.InitDBLockSeat(s)
}

