package model

import (
	"bytes"
	"encoding/json"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/giabao.ong/bms/core"
	"io"
)

type Cinema struct {
	ID          *bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	Size        Size           `json:"size,omitempty" bson:"size,omitempty"`
	MinDistance *int           `json:"min_distance,omitempty" bson:"min_distance,omitempty"`
	Available   *int           `json:"available,omitempty" bson:"available,omitempty"`
	Booked      *int           `json:"booked,omitempty" bson:"booked,omitempty"`
}

type Size struct {
	Length int `json:"length,omitempty" bson:"length,omitempty"`
	Width  int `json:"width,omitempty" bson:"width,omitempty"`
}

type InputCinema struct {
	Length      int `json:"length"`
	Width       int `json:"width"`
	MinDistance int `json:"min_distance"`
}

var DBCinema = &core.DBModel{
	TemplateObject: Cinema{},
	ColName:        "cinema",
}

func (cinema *InputCinema) VerifyInput() *core.Response {
	if cinema.Length <= 0 {
		return &core.Response{
			Status: core.APIStatus.Invalid,
			Message: "Length must be > 0",
		}
	} else if cinema.Width <= 0 {
		return &core.Response{
			Status: core.APIStatus.Invalid,
			Message: "Width must be > 0",
		}
	} else if cinema.MinDistance <= 0 {
		return &core.Response{
			Status: core.APIStatus.Invalid,
			Message: "MinDistance must be > 0",
		}
	}

	return &core.Response{
		Status:    core.APIStatus.Ok,
	}
}

func (cinema *InputCinema) ToIOReader() io.Reader {
	byteArr, _ := json.Marshal(cinema)
	return bytes.NewReader(byteArr)
}