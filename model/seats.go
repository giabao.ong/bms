package model

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/giabao.ong/bms/core"
)

var (
	EnumStatusSeat = &StatusSeat{
		Available:    "AVAILABLE",
		IsProcessing: "IS_PROCESSING",
		Booked:       "BOOKED",
	}

	DBSeat = &core.DBModel{
		DBName:         "cinema",
		TemplateObject: Seat{},
		ColName:        "seat",
	}
)

type Seat struct {
	Col      *int           `json:"col,omitempty" bson:"col,omitempty"`
	Row      *int           `json:"row,omitempty" bson:"row,omitempty"`
	Status   string         `json:"status,omitempty" bson:"status,omitempty"`
	TicketID *bson.ObjectId `json:"ticket_id,omitempty" bson:"ticket_id,omitempty"`
}

type StatusSeat struct {
	Available    string
	IsProcessing string
	Booked       string
}

type ListSeatsBooked struct {
	Col   int   `json:"col"`
	Row   int   `json:"row"`
	Error Error `json:"error"`
}

func InitDBSeat(s *core.DBSession) {
	DBSeat.Init(s)

	DBSeat.CreateIndex(mgo.Index{
		Key:        []string{"col", "row"},
		Background: true,
		Unique:     true,
	})

	DBSeat.CreateIndex(mgo.Index{
		Key:        []string{"status"},
		Background: true,
	})

	DBSeat.CreateIndex(mgo.Index{
		Key:        []string{"ticket_id"},
		Background: true,
	})
}

func NewSeat(col, row int) *core.Response {
	newSeat := Seat{
		Status: EnumStatusSeat.Available,
		Col:    &col,
		Row:    &row,
	}

	return DBSeat.Create(newSeat)
}
