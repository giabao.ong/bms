package model

type Error struct {
	Code    string
	Message string
}

type enumError struct {
	ParseFail           Error
	SeatInProcess		Error
	SeatIsBooked        Error
	SeatNotAccepted     Error
	DistanceNotAccepted Error
	InputInvalid        Error
}

var EnumError = &enumError{
	ParseFail: Error{
		Code:    "PARSE_DATA_FAILED",
		Message: "Parse data failed",
	},
	SeatInProcess: Error{
		Code:    "SEAT_IN_PROCESS",
		Message: "Invalid, [%d,%d] is in process",
	},
	SeatIsBooked: Error{
		Code:    "SEAT_INVALID",
		Message: "Invalid seat or booked",
	},
	SeatNotAccepted: Error{
		Code:    "SEAT_INVALID",
		Message: "Seat [%d,%d] is booked",
	},
	DistanceNotAccepted: Error{
		Code:    "SEAT_INVALID",
		Message: "The distance is accepted. Must be %d",
	},
	InputInvalid: Error{
		Code:    "INPUT_INVALID",
		Message: "Your input is invalid",
	},
}
