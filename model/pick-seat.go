package model

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/giabao.ong/bms/core"
	"time"
)

type PickSeat struct {
	Col         int            `json:"-" bson:"col,omitempty"`
	Row         int            `json:"-" bson:"row,omitempty"`
	TicketID    *bson.ObjectId `json:"-" bson:"ticket_id,omitempty"`
	CreatedTime *time.Time     `json:"-" bson:"created_time,omitempty"`
}

var DBLockSeat = &core.DBModel{
	DBName:         "cinema",
	TemplateObject: PickSeat{},
	ColName:        "pick_seat",
}

func InitDBLockSeat(s *core.DBSession) {
	DBLockSeat.Init(s)

	DBLockSeat.CreateIndex(mgo.Index{
		Key:      []string{"col", "row"},
		Unique:   true,
		DropDups: true,
	})

	DBLockSeat.CreateIndex(mgo.Index{
		Key:         []string{"created_time"},
		ExpireAfter: 4 * time.Minute,
	})
}
