package model

import (
	"bytes"
	"encoding/json"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/giabao.ong/bms/core"
	"io"
)

type Ticket struct {
	ID      *bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	Total   int            `json:"total,omitempty" bson:"total,omitempty"`
	FromRow *int           `json:"from_row,omitempty" bson:"from_row,omitempty"`
	ToRow   *int           `json:"to_row,omitempty" bson:"to_row,omitempty"`
	FromCol *int           `json:"from_col,omitempty" bson:"from_col,omitempty"`
	ToCol   *int           `json:"to_col,omitempty" bson:"to_col,omitempty"`
	IsGroup *bool          `json:"is_group,omitempty" bson:"is_group,omitempty"`
}

type BookTicket struct {
	Total   int  `json:"total,omitempty" bson:"total,omitempty"`
	FromRow int  `json:"from_row,omitempty" bson:"from_row,omitempty"`
	ToRow   int  `json:"to_row,omitempty" bson:"to_row,omitempty"`
	FromCol int  `json:"from_col,omitempty" bson:"from_col,omitempty"`
	ToCol   int  `json:"to_col,omitempty" bson:"to_col,omitempty"`
	IsGroup bool `json:"is_group,omitempty" bson:"is_group,omitempty"`
}

var DBTicket = &core.DBModel{
	DBName:         "cinema",
	TemplateObject: Ticket{},
	ColName:        "ticket",
}

func (b *BookTicket) VerifyInput() *core.Response {
	if b.FromCol < 0 || b.FromCol > b.ToCol {
		return &core.Response{
			Status:    core.APIStatus.Invalid,
			Message:   "From Column must be >=0 and greater To Column",
		}
	}else if b.FromRow < 0 || b.FromRow > b.ToRow {
		return &core.Response{
			Status:    core.APIStatus.Invalid,
			Message:   "From Row must be >=0 and greater To Row",
		}
	}

	if b.FromCol != b.ToCol {
		b.IsGroup = true
	}

	return &core.Response{
		Status:    core.APIStatus.Ok,
	}
}

func (b *BookTicket) ToIOReader() io.Reader {
	byteArr, _ := json.Marshal(b)
	return bytes.NewReader(byteArr)
}