package action

import (
	"github.com/globalsign/mgo/bson"
	"gitlab.com/giabao.ong/bms/core"
	"gitlab.com/giabao.ong/bms/model"
)

func BookTicket(input *model.BookTicket) *core.Response {
	var (
		total   = 0
		toCol   = input.ToCol
		fromCol = input.FromCol
	)

	resultCinema := model.DBCinema.Query(bson.M{}, 1, 0, false)
	if resultCinema.Status != core.APIStatus.Ok {
		return resultCinema
	}

	dataCinema, ok := resultCinema.Data.([]model.Cinema)
	if !ok {
		return &core.Response{
			Status:  core.APIStatus.Error,
			Message: "Parse data fail",
		}
	}

	cinema := dataCinema[0]
	id := bson.NewObjectId() // create id ticket

	defer func(tickID *bson.ObjectId) {
		ClearLock(tickID)
	}(&id)

	check := PickSeat(input, &cinema, &id)
	if check.Status != core.APIStatus.Ok {
		//Is process, do not revert
		return check
	}

	if input.FromRow != input.ToRow {
		toCol = cinema.Size.Width
	}
	for i := input.FromRow; i <= input.ToRow; {
		check = CheckSeatsIsValid(fromCol, toCol, i)
		if check.Status != core.APIStatus.Ok {
			return check
		}

		for j := fromCol; j <= toCol; j++ {
			res := CheckDistanceTop(i, j, &cinema)
			if res.Status != core.APIStatus.Ok {
				return res
			}

			if i < cinema.Size.Length {
				res = CheckDistanceBottom(i, j, &cinema)
				if res.Status != core.APIStatus.Ok {
					return res
				}
			}

			total++
		}

		i++
		if input.FromRow != input.ToRow {
			fromCol = 0
		}

		if i == input.ToRow {
			toCol = input.ToCol
		}
	}

	ticket := model.Ticket{
		ID:      &id,
		Total:   total,
		FromRow: &input.FromRow,
		ToRow:   &input.ToRow,
		FromCol: &input.FromCol,
		ToCol:   &input.ToCol,
		IsGroup: &input.IsGroup,
	}
	createTicket := model.DBTicket.Create(ticket)
	if createTicket.Status != core.APIStatus.Ok {
		return createTicket
	}

	res := BookedSeat(&ticket, input, &cinema)
	if res.Status == core.APIStatus.Ok {
		res.Message = "Booked success"
		defer CountSeats()
	}

	return res
}
