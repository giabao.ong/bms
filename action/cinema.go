package action

import (
	"github.com/globalsign/mgo/bson"
	"gitlab.com/giabao.ong/bms/core"
	"gitlab.com/giabao.ong/bms/model"
)

type ActionCinema struct{}

func (cinema ActionCinema) SetUp(inputCinema model.InputCinema) *core.Response {
	var res *core.Response

	filter := model.DBCinema.Query(bson.M{}, 1, 0, false)

	switch filter.Status {

	case core.APIStatus.Ok:
		updater := model.Cinema{
			MinDistance: &inputCinema.MinDistance,
		}

		res = model.DBCinema.UpdateOne(bson.M{},updater)
		res.Message = "Do not create, just update min distance"

	case core.APIStatus.NotFound:
		total := inputCinema.Width * inputCinema.Length

		newCinema := model.Cinema{
			Size: model.Size{
				Length: inputCinema.Length,
				Width:  inputCinema.Width,
			},
			MinDistance: &inputCinema.MinDistance,
			Available:   &total,
			Booked:      &model.Zero,
		}
		res = model.DBCinema.Create(newCinema)

		for i := 0; i <= inputCinema.Length; i++ {
			for j := 0; j <= inputCinema.Width; j++ {
				model.NewSeat(j, i)
			}
		}
	}

	return res
}

func (cinema ActionCinema) GetInfo() *core.Response {
	return model.DBCinema.Query(nil, 1, 0, false)
}

func CountSeats() *core.Response {
	query := model.Seat{
		Status: model.EnumStatusSeat.Available,
	}
	resultAvailable := model.DBSeat.Count(query)

	query = model.Seat{
		Status: model.EnumStatusSeat.Booked,
	}
	resultBook := model.DBSeat.Count(query)

	updater := model.Cinema{
		Available: &resultAvailable.Total,
		Booked:    &resultBook.Total,
	}
	res := model.DBCinema.UpdateOne(nil, updater)
	return res
}

func GetSeats(limit, offset int, reverse bool) *core.Response {
	query := model.Seat{
		Status: model.EnumStatusSeat.Available,
	}
	seats := model.DBSeat.Query(query, limit, offset, reverse)
	return seats
}