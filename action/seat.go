package action

import (
	"fmt"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/giabao.ong/bms/core"
	"gitlab.com/giabao.ong/bms/model"
)

//This function will lock current seat if it is not processing
func PickSeat(input *model.BookTicket, cinema *model.Cinema, id *bson.ObjectId) *core.Response {
	var (
		startCol  = input.FromCol
		targetCol = input.ToCol
	)

	if input.FromRow != input.ToRow {
		targetCol = cinema.Size.Width
	}

	for i := input.FromRow; i <= input.ToRow; {
		for j := startCol; j <= targetCol; j++ {
			err := model.DBLockSeat.Create(model.PickSeat{
				Col:      j,
				Row:      i,
				TicketID: id,
			})
			if err.Status != core.APIStatus.Ok {
				return &core.Response{
					Status:  core.APIStatus.Error,
					Message: fmt.Sprintf("Seat [%d,%d] is processing", j, i),
				}
			}
		}

		i++
		if input.FromRow != input.ToRow {
			startCol = 0
		}

		if i == input.ToRow {
			targetCol = input.ToCol
		}
	}

	return &core.Response{Status: core.APIStatus.Ok}
}

func ClearLock(id *bson.ObjectId) {
	model.DBLockSeat.Delete(model.PickSeat{
		TicketID: id,
	})
}

func CheckDistanceTop(row, col int, cinema *model.Cinema) *core.Response {
	//TODO find seats nearest this
	query := bson.M{
		"row":    bson.M{"$lte": row},
		"col":    bson.M{"$lte": col},
		"status": model.EnumStatusSeat.Booked,
	}
	res := model.DBSeat.QuerySort(query, 1, 0, "+row", "-col")
	if res.Status == core.APIStatus.Ok {
		data, ok := res.Data.([]model.Seat)
		if !ok {
			return &core.Response{
				Status:    core.APIStatus.Error,
				Message:   model.EnumError.ParseFail.Message,
				ErrorCode: model.EnumError.ParseFail.Code,
			}
		}

		seat := data[0]

		disToSeatCol := col - *seat.Col
		disToSeatRow := row - *seat.Row
		min := disToSeatCol + disToSeatRow
		if min < *cinema.MinDistance {
			return &core.Response{
				Status:    core.APIStatus.Error,
				Message:   fmt.Sprintf(model.EnumError.DistanceNotAccepted.Message, *cinema.MinDistance),
				ErrorCode: model.EnumError.DistanceNotAccepted.Code,
			}
		}
	}

	if col < cinema.Size.Width {
		query := bson.M{
			"row":    bson.M{"$lte": row},
			"col":    bson.M{"$gte": col},
			"status": model.EnumStatusSeat.Booked,
		}
		res := model.DBSeat.QuerySort(query, 1, 0, "+row", "+col")
		if res.Status == core.APIStatus.Ok {
			data, ok := res.Data.([]model.Seat)
			if !ok {
				return &core.Response{
					Status:    core.APIStatus.Error,
					Message:   model.EnumError.ParseFail.Message,
					ErrorCode: model.EnumError.ParseFail.Code,
				}
			}

			seat := data[0]

			disToSeatCol := *seat.Col - col
			disToSeatRow := row - *seat.Row

			min := disToSeatCol + disToSeatRow

			if min < *cinema.MinDistance {
				return &core.Response{
					Status:    core.APIStatus.Error,
					Message:   fmt.Sprintf(model.EnumError.DistanceNotAccepted.Message, *cinema.MinDistance),
					ErrorCode: model.EnumError.DistanceNotAccepted.Code,
				}
			}
		}
	}

	return &core.Response{
		Status: core.APIStatus.Ok,
	}
}

func CheckDistanceBottom(row, col int, cinema *model.Cinema) *core.Response {
	query := bson.M{
		"row":    bson.M{"$gt": row},
		"col":    bson.M{"$lte": col},
		"status": model.EnumStatusSeat.Booked,
	}
	res := model.DBSeat.QuerySort(query, 1, 0, "+row", "-col")
	if res.Status == core.APIStatus.Ok {
		data, ok := res.Data.([]model.Seat)
		if !ok {
			return &core.Response{
				Status:    core.APIStatus.Error,
				Message:   model.EnumError.ParseFail.Message,
				ErrorCode: model.EnumError.ParseFail.Code,
			}
		}

		seat := data[0]

		disToSeatCol := col - *seat.Col
		disToSeatRow := *seat.Row - row
		min := disToSeatCol + disToSeatRow
		if min < *cinema.MinDistance {
			return &core.Response{
				Status:    core.APIStatus.Error,
				Message:   fmt.Sprintf(model.EnumError.DistanceNotAccepted.Message, *cinema.MinDistance),
				ErrorCode: model.EnumError.DistanceNotAccepted.Code,
			}
		}
	}

	if col < cinema.Size.Width {
		query := bson.M{
			"row":    bson.M{"$gt": row},
			"col":    bson.M{"$gte": col},
			"status": model.EnumStatusSeat.Booked,
		}
		res := model.DBSeat.QuerySort(query, 1, 0, "+row", "+col")
		if res.Status == core.APIStatus.Ok {
			data, ok := res.Data.([]model.Seat)
			if !ok {
				return &core.Response{
					Status:    core.APIStatus.Error,
					Message:   model.EnumError.ParseFail.Message,
					ErrorCode: model.EnumError.ParseFail.Code,
				}
			}

			seat := data[0]

			disToSeatCol := *seat.Col - col
			disToSeatRow := *seat.Row - row
			min := disToSeatCol + disToSeatRow
			if min < *cinema.MinDistance {
				return &core.Response{
					Status:    core.APIStatus.Error,
					Message:   fmt.Sprintf(model.EnumError.DistanceNotAccepted.Message, *cinema.MinDistance),
					ErrorCode: model.EnumError.DistanceNotAccepted.Code,
				}
			}
		}
	}

	return &core.Response{
		Status: core.APIStatus.Ok,
	}
}

func CheckSeatsIsValid(fromCol, toCol, row int) *core.Response {
	// Check current request if it has one or many seats are booked. This req not accept
	query := bson.M{
		"$and": []bson.M{
			{"col": bson.M{"$lte": toCol}},
			{"col": bson.M{"$gte": fromCol}},
			{"row": row},
			{"status": model.EnumStatusSeat.Booked},
		},
	}
	filter := model.DBSeat.Query(query, 1, 0, false)
	if filter.Status == core.APIStatus.Ok {
		dataSeats, ok := filter.Data.([]model.Seat)
		if ok {
			var listErrs []model.ListSeatsBooked
			for _, v := range dataSeats {
				err := model.ListSeatsBooked{
					Row:   *v.Row,
					Col:   *v.Col,
					Error: model.EnumError.SeatIsBooked,
				}

				listErrs = append(listErrs, err)
			}

			return &core.Response{
				Status: core.APIStatus.Error,
				Message: model.EnumError.SeatIsBooked.Message,
				ErrorCode: model.EnumError.SeatIsBooked.Code,
				Data: listErrs,
			}
		}
	}

	return &core.Response{Status: core.APIStatus.Ok}
}

func BookedSeat(ticket *model.Ticket, input *model.BookTicket, cinema *model.Cinema) *core.Response {
	var (
		targetCol = input.ToCol
		startCol  = input.FromCol
		res       *core.Response
	)

	if input.FromRow != input.ToRow {
		startCol = input.FromCol
		targetCol = cinema.Size.Width
	}

	for i := input.FromRow; i <= input.ToRow; {
		for j := startCol; j <= targetCol; j++ {
			query := model.Seat{
				Col:    &j,
				Row:    &i,
				Status: model.EnumStatusSeat.Available,
			}

			res = model.DBSeat.UpdateOne(query, model.Seat{
				Status:   model.EnumStatusSeat.Booked,
				TicketID: ticket.ID,
			})
		}

		i++
		if input.FromRow != input.ToRow {
			startCol = 0
		}

		if i == input.ToRow {
			targetCol = input.ToCol
		}
	}

	return res
}
