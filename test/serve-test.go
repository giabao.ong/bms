package test

import (
	"gitlab.com/giabao.ong/bms/config"
	"gitlab.com/giabao.ong/bms/core"
)

func NewServeTest() {
	app := core.App{
		AppName: "Booking management",
	}

	newDB := core.ConfigDB{
		DBName: "cinema_test",
	}

	app.DBInit(&newDB, config.OnDBConnected)
	app.Launch()
	println("Launch Test")
}
