package test

import (
	"gitlab.com/giabao.ong/bms/api"
	"gitlab.com/giabao.ong/bms/core"
	"gitlab.com/giabao.ong/bms/model"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
)

func init() {
	NewServeTest()
}

func TestRunner(t *testing.T) {
	t.Run("Scenario 1: Check input create/config cinema", func(t *testing.T) {
		testSetUpCinema(t, model.InputCinema{
			Length:      20,
			Width:       15,
			MinDistance: 3,
		})
	})

	t.Run("Scenario 2: Get cinema information", func(t *testing.T) {
		testGetCinemaInfo(t)
	})

	t.Run("Scenario 3: Book seats successful", func(t *testing.T) {
		testBookSeats(t, model.BookTicket{
			Total:   3,
			FromRow: 0,
			ToRow:   0,
			FromCol: 1,
			ToCol:   3,
			IsGroup: false,
		}, "SUCCESS")
	})

	t.Run("Scenario 4: Book seats failed. Because min distance is not acceptable", func(t *testing.T) {
		testBookSeats(t, model.BookTicket{
			Total:   1,
			FromRow: 0,
			ToRow:   0,
			FromCol: 4,
			ToCol:   4,
			IsGroup: false,
		}, "FAILED")

		testBookSeats(t, model.BookTicket{
			Total:   1,
			FromRow: 2,
			ToRow:   2,
			FromCol: 3,
			ToCol:   3,
			IsGroup: false,
		}, "FAILED")
	})

	t.Run("Scenario 5: Book seats failed. Because this seats is booked", func(t *testing.T) {
		testBookSeats(t, model.BookTicket{
			Total:   3,
			FromRow: 0,
			ToRow:   0,
			FromCol: 0,
			ToCol:   2,
			IsGroup: false,
		}, "FAILED")
	})

	t.Run("Scenario 6: Test book with high request", func(t *testing.T) {
		testBookWithAmountOfRequest(t)
	})
}

func testSetUpCinema(t *testing.T, inputTest model.InputCinema) {
	check := inputTest.VerifyInput()
	if check.Status != core.APIStatus.Ok {
		t.Error(check.Message)
		return
	}

	url := "/bms/v1/cinema/set-up"
	request, _ := http.NewRequest(http.MethodPost, url, inputTest.ToIOReader())
	reponse := httptest.NewRecorder()

	req, resp := core.NewMethodAPIForTest(request, reponse)

	api.SetUpCinema(req, resp)
	if resp.GetHTTPCode() != 200 {
		t.Error(resp.GetHTTPCode())
	}
}

func testGetCinemaInfo(t *testing.T) {
	url := "/bms/v1/cinema/info"
	request, _ := http.NewRequest(http.MethodGet, url, nil)
	reponse := httptest.NewRecorder()

	req, resp := core.NewMethodAPIForTest(request, reponse)

	api.GetInfo(req, resp)
	if resp.GetHTTPCode() != 200 {
		t.Error(resp.GetHTTPCode())
	}
}

func testBookSeats(t *testing.T, input model.BookTicket, expect string) {
	check := input.VerifyInput()
	if check.Status != core.APIStatus.Ok {
		t.Error(check.Message)
	}

	url := "/bms/v1/ticket/book"
	request, _ := http.NewRequest(http.MethodPost, url, input.ToIOReader())
	reponse := httptest.NewRecorder()

	req, resp := core.NewMethodAPIForTest(request, reponse)

	api.BookTicket(req, resp)

	switch expect {
	case "SUCCESS":
		if resp.GetHTTPCode() != 200 {
			t.Error(resp.GetHTTPCode(), resp.GetResponse().Message)
		}
	case "FAILED":
		if resp.GetHTTPCode() == 200 {
			t.Error("Action must be fail ", resp.GetHTTPCode(), resp.GetResponse())
		}
	}
}

func testBookWithAmountOfRequest(t *testing.T) {
	var (
		data   []model.BookTicket
		result = make(map[string]int)
	)
	input := model.BookTicket{
		Total:   4,
		FromRow: 4,
		ToRow:   4,
		FromCol: 0,
		ToCol:   3,
		IsGroup: true,
	}
	data = append(data, input)

	input = model.BookTicket{
		FromRow: 7,
		ToRow:   8,
		FromCol: 4,
		ToCol:   6,
		IsGroup: true,
	}

	data = append(data, input)

	for i := 0; i < 1000; i++ {
		input = model.BookTicket{
			FromRow: 13,
			ToRow:   13,
			FromCol: 7,
			ToCol:   9,
			IsGroup: true,
		}
		data = append(data, input)
	}

	wg := &sync.WaitGroup{}
	mux := &sync.Mutex{}
	wg.Add(len(data))
	for _, v := range data {
		go bookSeatsWithGo(v, wg, mux, result)
	}
	wg.Wait()

	if result["200"] != 3 {
		t.Error(result["200"])
	}

	if result["!200"] != 999 {
		t.Error(result["!200"])
	}
}

func bookSeatsWithGo(input model.BookTicket, wg *sync.WaitGroup, mutex *sync.Mutex, result map[string]int) {
	url := "/bms/v1/ticket/book"
	request, _ := http.NewRequest(http.MethodPost, url, input.ToIOReader())
	reponse := httptest.NewRecorder()

	req, resp := core.NewMethodAPIForTest(request, reponse)

	api.BookTicket(req, resp)

	mutex.Lock()
	if resp.GetHTTPCode() != 200 {
		result["!200"] = result["!200"] + 1
	} else {
		result["200"] = result["200"] + 1
	}
	mutex.Unlock()

	wg.Done()
}
