package api

import (
	"gitlab.com/giabao.ong/bms/action"
	"gitlab.com/giabao.ong/bms/core"
	"gitlab.com/giabao.ong/bms/model"
)

func BookTicket(req core.APIRequest, resp core.APIResponse) error {
	input := model.BookTicket{}
	err := req.ParseBodyTo(&input)
	if err != nil {
		return resp.Resp(&core.Response{
			Status:  core.APIStatus.Invalid,
			Message: err.Error(),
		})
	}

	check := input.VerifyInput()
	if check.Status != core.APIStatus.Ok {
		return resp.Resp(check)
	}

	res := action.BookTicket(&input)
	return resp.Resp(res)
}
