package api

import (
	"fmt"
	"gitlab.com/giabao.ong/bms/action"
	"gitlab.com/giabao.ong/bms/core"
	"gitlab.com/giabao.ong/bms/model"
	"strconv"
)

func SetUpCinema(req core.APIRequest, resp core.APIResponse) error {
	input := model.InputCinema{}

	err := req.ParseBodyTo(&input)
	if err != nil {
		return resp.Resp(&core.Response{
			Status:    core.APIStatus.Invalid,
			Message:   err.Error(),
		})
	}

	check := input.VerifyInput()
	if check.Status != core.APIStatus.Ok {
		return resp.Resp(check)
	}

	res := action.ActionCinema{}.SetUp(input)
	return resp.Resp(res)
}

func GetInfo(req core.APIRequest, resp core.APIResponse) error {
	res := action.ActionCinema{}.GetInfo()
	return resp.Resp(res)
}

func GetSeats(req core.APIRequest, resp core.APIResponse) error {
	defer func(response core.APIResponse) {
		fmt.Println(resp.GetHTTPCode())
	}(resp)
	limit, err := strconv.Atoi(req.GetParam("limit"))
	if err != nil {
		limit = 1
	}

	offset, err := strconv.Atoi(req.GetParam("offset"))
	if err != nil {
		offset = 0
	}

	reverse := false
	if req.GetParam("offset") == "true" {
		reverse = true
	}

	res := action.GetSeats(limit, offset, reverse)
	return resp.Resp(res)
}