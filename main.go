package main

import (
	"fmt"
	"gitlab.com/giabao.ong/bms/api"
	"gitlab.com/giabao.ong/bms/config"
	"gitlab.com/giabao.ong/bms/core"
	"os"
)

var serviceName = "bms"
var version = "v1"

func genUrl(path string) string {
	return fmt.Sprintf("/%s/%s/%s", serviceName, version, path)
}

func main() {
	app := core.App{
		AppName: "Booking management",
	}

	newDB := core.ConfigDB{
		DBName: "cinema",
	}
	app.DBInit(&newDB, config.OnDBConnected)
	//serve := app.SetupProtocolServer("gRPC") //enable this if you want run on gRPC
	protocol := os.Getenv("protocol")
	if protocol == "" {
		protocol = "HTTP"
	}
	serve := app.SetupProtocolServer(protocol)
	serve.SetHandler(core.APIMethod.GET, genUrl("api-info"), ApiInfo)
	serve.SetHandler(core.APIMethod.GET, genUrl("cinema/info"), api.GetInfo)
	serve.SetHandler(core.APIMethod.GET, genUrl("cinema/get-seats"), api.GetSeats)
	serve.SetHandler(core.APIMethod.POST, genUrl("cinema/set-up"), api.SetUpCinema)
	serve.SetHandler(core.APIMethod.POST, genUrl("ticket/book"), api.BookTicket)

	serve.SetPort(80)
	app.Launch()
}

func ApiInfo(request core.APIRequest, resp core.APIResponse) error {
	return resp.Resp(&core.Response{
		Status:    core.APIStatus.Ok,
		Message:   "Hello to work",
		ErrorCode: "",
	})
}