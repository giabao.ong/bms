### How to run

* To run the test <br>
`go test -v ./test`

* To get deps <br>
`go mod vendor`

* To run application <br> `go run main.go`